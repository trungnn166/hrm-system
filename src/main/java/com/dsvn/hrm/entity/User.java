package com.dsvn.hrm.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="user")
@Data

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
}
